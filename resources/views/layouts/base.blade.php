<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema Contable</title>
    <meta name="description" content="Sistema contable">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('partials.head')
</head>
<body class="gray-bg">
    @yield('content')
    @include('partials.script')
</body>
</html>